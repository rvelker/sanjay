package category_file;

import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class CategoryFileSetupTeardown {

    static FileInputStream getFile() {
        String fileLocation = "C:\\Projects\\Sanjay\\src\\main\\data\\Category File (SEPARATED).xlsx";
        FileInputStream file = null;

        try {
            file = new FileInputStream(new File(fileLocation));

        } catch (FileNotFoundException e) {
            System.out.println("Categories File not found");
//            Errors.categoriesFileNotFound(fileLocation);

        } catch (Exception e2) {
            System.out.println("E2");
            //todo
        }
        return file;
    }

    static Workbook createWorkbook(FileInputStream file) {
        XSSFWorkbook workbook = null;

        if (file != null) {

            try {
                workbook = new XSSFWorkbook(file);
            } catch (IOException e) {
                System.out.println("Failed to init workbook");

                e.printStackTrace();
            }
        }
        return workbook;
    }
    //todo
    static void teardown(FileInputStream file, Workbook workbook) {
        try {
            file.close();
            System.out.println("File closed successfully");

        } catch (IOException e) {
            System.out.println("Failed to close file input stream");
        }
        try {
            workbook.close();
            System.out.println("Workbook closed successfully");

        } catch (IOException e) {
            System.out.println("Failed to close workbook");
            e.printStackTrace();
        }
    }

}
