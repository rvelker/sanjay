package category_file;

import myObjects.Category;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

public class CategoryRowReader {

    static Category readCategory(Row row, int columns) {
        int rowNumber = row.getRowNum() + 1;

//        System.out.println("Reading row " + rowNumber);

        Cell cell;
        boolean rowIsEmpty = true;

        String parent = "";
        String cat0 = "";
        String cat1 = "";
        String cat2 = "";
        String id = "";


        for (int i = 0; i < columns; i++) {

            cell = row.getCell(i, Row.MissingCellPolicy.CREATE_NULL_AS_BLANK);
            String text = cell.toString().trim();

            if (!text.equals("")) { rowIsEmpty = false; }

            switch (i) {

                case 0 : parent = text;

                case 1 : cat0 = text;

                case 2 : cat1 = text;

                case 3 : cat2 = text;
            }

            if (i == columns - 1) { id = text; }
        }


        if (rowIsEmpty) {
            System.out.println("Row is empty at:" + rowNumber);
            System.out.println("------------------------------");
            return null;

        } else if (categoryHasInvalidEmptyValues(rowNumber, parent, cat0, cat1, cat2, id)) {
            return null;
        }
        return new Category(parent, cat0, cat1, cat2, id);
    }

    private static boolean categoryHasInvalidEmptyValues(int rowNumber, String parent, String cat0, String cat1, String cat2, String id) {
        if ((parent.equals(""))) {
            System.out.println("Failed to extract main category name from row: --" + rowNumber + "--");
            System.out.println("-------------------------------------------");
            return true;
        }

        if (cat0.equals("") && cat1.equals("") && cat2.equals("")) {
            System.out.println("Failed to extract sub-category names from row --" + rowNumber + "--");
            System.out.println("---------------------------------------------");
            return true;
        }

        if (id.equals("")) {
            System.out.println("Failed to extract category tree value from row --" + rowNumber + "--");
            System.out.println("---------------------------------------------");
            return true;
        }
        return false;
    }

}
