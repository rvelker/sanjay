package category_file;

import myObjects.CategorySheet;
import myObjects.Category;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import java.io.FileInputStream;
import java.util.*;

public class CategoryFileReader {

    public static List<CategorySheet> getCategories() {
        FileInputStream file = CategoryFileSetupTeardown.getFile();
        Workbook workbook = CategoryFileSetupTeardown.createWorkbook(file);
        List<CategorySheet> data = readFile(workbook);
        CategoryFileSetupTeardown.teardown(file, workbook);
        return data;
    }

    public static List<CategorySheet> readFile(Workbook wb) {
        try {
            int sheets = wb.getNumberOfSheets();
            System.out.println("Number of sheets = " + sheets);
            List<CategorySheet> categories = new ArrayList<>();

            for (int i = 1; i < sheets; i++) {
                CategorySheet sheetData = readSheet(i, wb);
                categories.add(sheetData);
            }
            return categories;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private static CategorySheet readSheet(int i, Workbook wb) {
        Sheet sheet = wb.getSheetAt(i);
        System.out.println("Reading sheet " + sheet.getSheetName());
        int columns = 5;

        Iterator<Row> rows = sheet.rowIterator();
        List<Category> categories = new ArrayList<>();

        while (rows.hasNext()) {
            Row row = rows.next();

            if (i == 1 && row.getRowNum() == 0) { continue; }

            Category category = CategoryRowReader.readCategory(row, columns);

            if (category != null) {
                categories.add(category);
//                System.out.println("Categories list for sheet --" + sheet.getSheetName() + "-- has size = " + categories.size());
//                System.out.println("Parent = " + category.getParentName());
//                System.out.println("Cat 0 = " + category.getCat0());
//                System.out.println("Cat 1 = " + category.getCat1());
//                System.out.println("Cat 2 = " + category.getCat2());
//                System.out.println("ID = " + category.getId());
//                System.out.println("----------------------------");
            }
        }
        System.out.println("Categories list for sheet --" + sheet.getSheetName() + "-- has size = " + categories.size());
        return new CategorySheet(categories);
    }
}
