package products_file;

import myObjects.ParentEndpointPair;
import myObjects.Product;
import org.apache.maven.shared.utils.StringUtils;

import java.util.*;


public class ParentEndpointExtractor {

    public static void extractCategoryEndpoints(List<Product> products) {

        for (Product product : products) {
            String categoriesString = product.getComments();
            String[] individualCategoriesArray = categoriesString.split("\\n");
            List<List<String>> separatedCategories = separateCategories(individualCategoriesArray);
            List<ParentEndpointPair> pairs = getParentEndpointPairs(separatedCategories);
            product.setParentEndpointPairs(pairs);
        }
    }


    private static List<ParentEndpointPair> getParentEndpointPairs(List<List<String>> separatedCategories) {
        List<ParentEndpointPair> pairs = new ArrayList<>();

        for (List<String> category : separatedCategories) {
            List<String> elementsInThisCategory = new ArrayList<>();

            for (String line : category) {
                String[] elements = line.split(">");

                for (String element : elements) {
                    elementsInThisCategory.add(element.trim());
                }
            }

            for (String element : elementsInThisCategory) {

                if (Collections.frequency(elementsInThisCategory, element) == 1) {
                    String parent = category.get(category.size() - 1);
                    ParentEndpointPair pair = new ParentEndpointPair(parent, element);
                    pairs.add(pair);
                    break;
                }
            }
        }
        return pairs;
    }

    private static List<List<String>> separateCategories(String[] categories) {
        List<List<String>> allCategories = new ArrayList<>();
        List<String> currentCategory = new ArrayList<>();

        for (String line : categories) {

            int separatorOccurrences = StringUtils.countMatches(line, ">");
            currentCategory.add(line);

            if (separatorOccurrences == 0) {
                List<String> fullCategory = new ArrayList<>(currentCategory);
                allCategories.add(fullCategory);
                currentCategory.clear();
            }
        }
        return allCategories;
    }
}
