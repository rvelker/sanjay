package products_file;

import errors.CustomErrorHandler;
import myObjects.Product;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

public class ProductsRowReader {

    static Product readProduct(Row row) {
        System.out.println("Reading row: " + row.getRowNum());

        String slotNumber = "";
        String name = "";
        String comments = "";

        int commentsColumn = 9;
        int emptyCells = 0;

        try {
            for (int i = 0; i <= commentsColumn; i++) {
                Cell cell = row.getCell(i, Row.MissingCellPolicy.CREATE_NULL_AS_BLANK);
                String cellText = cell.toString().trim();

                if (emptyCells < 2) {

                    if(cellText.equals("") && i < 2) {
                        emptyCells++;
                        continue;
                    }

                    switch (i) {
                        case 0: slotNumber = cellText.replace(".0","");
                        case 3: name = cellText;
                        case 9: comments = cellText;
                    }

                } else { return null; }
            }
        } catch (Exception e) {
            int rowNumber = row.getRowNum() + 1;
            CustomErrorHandler.error();
            return null;
        }

        Product product = new Product();
        product.setRowNumber(row.getRowNum() + 1);
        product.setSlotNumber(slotNumber);
        product.setName(name);
        product.setComments(comments);
        return product;
    }

}
