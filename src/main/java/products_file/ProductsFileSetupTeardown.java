package products_file;

import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.*;

public class ProductsFileSetupTeardown {

//    private static final String originalFileLocation = "C:\\Projects\\Sanjay\\src\\main\\data\\milestone-v1.1_Updated_b.xlsx";
//    private static final String newFileLocation = "C:\\Projects\\Sanjay\\src\\main\\data\\milestone-v1.1_Updated_b_FORMATTED.xlsx";

    private static final String originalFileLocation = "C:\\Projects\\Sanjay\\src\\main\\data\\Products test.xlsx";
    private static final String newFileLocation = "C:\\Projects\\Sanjay\\src\\main\\data\\Products test_FORMAT.xlsx";

    private static FileInputStream newFile;
    private static Workbook newWorkbook;

    static Workbook getFile() {
        copyProductsFile();
        newFile = createFileInputStream(newFileLocation);
        newWorkbook = createWorkbook(newFile);
        return newWorkbook;
    }

    private static void copyProductsFile() {
        FileInputStream originalFile = createFileInputStream(originalFileLocation);
        Workbook originalWorkbook = createWorkbook(originalFile);
        copyFile(originalWorkbook);
        teardown(originalFile, originalWorkbook);
    }

    private static FileInputStream createFileInputStream(String location) {
        FileInputStream file = null;

        try {
            file = new FileInputStream(new File(location));

        } catch (FileNotFoundException e) {
            System.out.println("New Products File not found");
//            Errors.categoriesFileNotFound(fileLocation);

        } catch (Exception e2) {
            System.out.println("E2");
            //todo
        }


        return file;
    }

    private static Workbook createWorkbook(FileInputStream file) {
        XSSFWorkbook workbook = null;

        if (file != null) {

            try {
                workbook = new XSSFWorkbook(file);
            } catch (IOException e) {
                System.out.println("Failed to init workbook");

                e.printStackTrace();
            }
        }
        return workbook;
    }

    private static void teardown(FileInputStream file, Workbook workbook) {
        try {
            file.close();
            System.out.println("File closed successfully");

        } catch (IOException e) {
            System.out.println("Failed to close file input stream");
        }
        try {
            workbook.close();
            System.out.println("Workbook closed successfully");

        } catch (IOException e) {
            System.out.println("Failed to close workbook");
            e.printStackTrace();
        }
    }

    static void teardownCopy() {
        teardown(newFile,newWorkbook);
    }

    private static void copyFile(Workbook workbook) {
        try {
            FileOutputStream outputStream = new FileOutputStream(newFileLocation);
            workbook.write(outputStream);
        } catch (IOException e) {
            System.out.println("Failed to copy products file");
            e.printStackTrace();
        }

    }

}
