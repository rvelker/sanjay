package products_file;

import myObjects.Product;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import java.util.*;

import errors.CustomErrorHandler;


public class ProductsFileReader{

    public static List<Product> getProducts() {
        Workbook wb = ProductsFileSetupTeardown.getFile();
        List<Product> products = readFile(wb);
        ProductsFileSetupTeardown.teardownCopy();
        return products;
    }

    public static List<Product> readFile(Workbook wb) {

        List<Product> products;
        Sheet sheet;

        try {
            sheet = wb.getSheetAt(0);
        } catch (Exception e) {
            CustomErrorHandler.error();
            return null;
        }

        try{
            products = readRows(sheet);
        } catch (Exception e) {
            CustomErrorHandler.error();
            return null;
        }
        return products;
    }

    private static List<Product> readRows(Sheet sheet){
        List<Product> products = new ArrayList<>();
        Iterator<Row> rows = sheet.rowIterator();
        boolean header = true;
        int items = 0;

        while (rows.hasNext() && items < 15) {
            Row row = rows.next();

            if (header) {
                header = false;
                continue;
            }

            Product product = ProductsRowReader.readProduct(row);
            if (product != null) {
                products.add(product);
                System.out.println("Added to list: " + product.getName());
                items++;
            }
        }
        return products;
    }

}