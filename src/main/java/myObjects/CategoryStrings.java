package myObjects;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CategoryStrings {

        public static final String highStreet = "High Street";
        public static final String healthFood = "Health Food";
        public static final String coffee = "Coffee";
        public static final String tea = "Tea";
        public static final String chocolate = "Chocolate";
        public static final String retro = "Retro";
        public static final String luxury = "Luxury";
        public static final String gifts = "Gifts";

        public static final String sheetHighStreet = "HIGHSTREET";
        public static final String sheetHealthFood = "HEALTHFOOD";
        public static final String sheetCoffee = "COFFEE";
        public static final String sheetTea = "TEA";
        public static final String sheetChocolate = "CHOCOLATE";
        public static final String sheetRest = "REST";
        public static final List<String> sheetNames = Arrays.asList(sheetHighStreet,sheetHealthFood,sheetCoffee,sheetTea,sheetChocolate,sheetRest);

}
