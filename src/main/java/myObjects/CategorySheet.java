package myObjects;

import java.util.List;

public class CategorySheet {

    public CategorySheet(List<Category> categories) {
        this.categories = categories;
        this.sheetName = categories.get(0).getParentName();
    }

    private String sheetName;
    private List<Category> categories;

    public List<Category> getCategories() {
        return categories;
    }

    public String getSheetName() {
        return sheetName;
    }
}
