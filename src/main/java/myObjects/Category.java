package myObjects;

public class Category {

    public Category(String parent, String cat0, String cat1, String cat2, String id) {
        this.parent = parent;
        this.cat0 = cat0;
        this.cat1 = cat1;
        this.cat2 = cat2;
        this.id = id;
    }

    private String parent;
    private String cat0;
    private String cat1;
    private String cat2;
    private String id;

    public String getParentName() {
        return parent;
    }

    public String getCat0() {
        return cat0;
    }

    public String getCat1() {
        return cat1;
    }

    public String getCat2() {
        return cat2;
    }

    public String getId() {
        return id;
    }
}
