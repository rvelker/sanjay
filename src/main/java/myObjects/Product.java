package myObjects;

import java.util.ArrayList;
import java.util.List;

public class Product {

    private int rowNumber = 0;
    private String slotNumber = "";
    private String name = "";
    private String comments = "";
    private String newCategory = "";
    private String highStreet = "";
    private String healthFood = "";
    private String coffee = "";
    private String tea = "";
    private List<ParentEndpointPair> parentEndpointPairs = new ArrayList<>();
    private String categoryId = "";

    public String getSlotNumber() {
        return slotNumber;
    }

    public void setSlotNumber(String slotNumber) {
        this.slotNumber = slotNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getNewCategory() {
        return newCategory;
    }

    public void setNewCategory(String newCategory) {
        this.newCategory = newCategory;
    }

    public String getHighStreet() {
        return highStreet;
    }

    public void setHighStreet(String highStreet) {
        this.highStreet = highStreet;
    }

    public String getHealthFood() {
        return healthFood;
    }

    public void setHealthFood(String healthFood) {
        this.healthFood = healthFood;
    }

    public String getCoffee() {
        return coffee;
    }

    public void setCoffee(String coffee) {
        this.coffee = coffee;
    }

    public String getTea() {
        return tea;
    }

    public void setTea(String tea) {
        this.tea = tea;
    }

    public int getRowNumber() {
        return rowNumber;
    }

    public void setRowNumber(int rowNumber) {
        this.rowNumber = rowNumber;
    }

    public List<ParentEndpointPair> getParentEndpointPairs() {
        return parentEndpointPairs;
    }

    public void setParentEndpointPairs(List<ParentEndpointPair> parentEndpointPairs) {
        this.parentEndpointPairs = parentEndpointPairs;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }
}
