package myObjects;

public class ParentEndpointPair {

    public ParentEndpointPair(String parent, String endpoint) {
        this.parent = parent;
        this.endpoint = endpoint;
    }

    private String parent;
    private String endpoint;

    public String getParent() {
        return parent;
    }

    public void setParent(String parent) {
        this.parent = parent;
    }

    public String getEndpoint() {
        return endpoint;
    }

    public void setEndpoint(String endpoint) {
        this.endpoint = endpoint;
    }
}
